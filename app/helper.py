from django.http import HttpResponse,HttpResponseRedirect,Http404
from django.shortcuts import render
from django.conf import settings
import json,os

def default_response():
    response             = {}
    response['status']     = False
    return response

def default_context():
    context = {}
    context['auth'] = True
    return context

def serialize_response(response):
    return HttpResponse(json.dumps(response, encoding="utf-8"), content_type='application/json')
import sys
from copy import copy
from optparse import OptionParser
from virus_total_API import VirusTotalAPI

#To scan url:
#python script.py scan -t url --url http://www.virustotal.com
usage  = "\n-To scan file:\n"
usage += "python script.py scan -t file --file_name raw.txt --file_path D:/SparkProjects/raw1.txt\n"
usage += 'WHERE: \n'
usage += '\t--file_name is the file name\n'
usage += '\t--file_path is the path of file to be submitted for scanning\n'

usage += "\n-To scan URL:\n" 
usage += "python script.py scan -t url --url http://www.virustotal.com\n"
usage += 'WHERE: \n'
usage += '\t--url is the url to be submitted for scanning\n'

usage += "\n-To retrieve file scan reports:\n" 
usage += "python script.py report -t file --resource f7ddc794cd38f4a4ad1b36d3bec5918e\n"
usage += 'WHERE: \n'
usage += "\t--resource can be md5/sha1/sha256 hash or scan_ids returned in reponse when file is submitted\n"

usage += "\n-To retrieve URL scan reports:\n" 
usage += "python script.py report -t url --resource http://www.virustotal.com\n"
usage += 'WHERE: \n'
usage += "\t--resource can be scan_ids returned in reponse when url is submitted for scanning\n"
usage += "\tor --resource can be the url as in above example \n"

usage += "\n-To retrieve IP scan reports:\n" 
usage += "python script.py report -t ip --resource 90.156.201.27\n"
usage += 'WHERE: \n'
usage += "\t--resource can be a valid IPv4 address in dotted quad notation, for the time being only IPv4 addresses are supported\n"

usage += "\n-To retrieve Domain scan reports:\n" 
usage += "python script.py report -t domain --resource 027.ru\n"
usage += 'WHERE: \n'
usage += "\t--resource is a domain name\n"

usage += "\n-To rescan a file:\n" 
usage += "python script.py rescan --resource f7ddc794cd38f4a4ad1b36d3bec5918e\n"
usage += 'WHERE: \n'
usage += "\t--resource can be md5/sha1/sha256 hash or scan_ids returned in reponse when file is initially submitted for scanning\n"


if __name__ == '__main__':
    vt = VirusTotalAPI()
    args = copy(sys.argv)
    cmd = ''

    try:
        cmd = args.pop(1)
    except:
        print usage

    if cmd == 'scan':
        #parser = OptionParser(usage=usage)
        parser = OptionParser()
        parser.add_option('-t', '--type', dest="type")
        parser.add_option('--file_name', dest="file_name")
        parser.add_option('--file_path', dest="file_path")
        parser.add_option('--url', dest="url")

        (opts, args) = parser.parse_args(args)

        if opts.type == 'file':
            if opts.file_name != None and opts.file_path != None:
                print vt.scan_file(opts.file_name, opts.file_path)
            else: print 'Parameters missing'
        elif opts.type == 'url':
            if opts.url != None:
                print vt.scan_url(opts.url)
            else: print 'Parameters missing'

    if cmd == 'report':
        parser = OptionParser()
        parser.add_option('-t', dest="type")
        parser.add_option('--resource', dest="resource")

        (opts, args) = parser.parse_args(args)

        if opts.type == 'file':
            if opts.resource != None:
                print vt.scan_file_report(opts.resource)
            else: print 'Parameters missing'
        elif opts.type == 'url':
            if opts.resource != None:
                print vt.scan_url_report(opts.resource)
            else: print 'Parameters missing'
        elif opts.type == 'ip':
            if opts.resource != None:
                print vt.scan_ip_report(opts.resource)
            else: print 'Parameters missing'
        elif opts.type == 'domain':
            if opts.resource != None:
                print vt.scan_domain_report(opts.resource)
            else: print 'Parameters missing'

    if cmd == 'rescan':
        parser = OptionParser()
        parser.add_option('--resource', dest="resource")

        (opts, args) = parser.parse_args(args)

        if opts.resource != None:
            print vt.rescan_file(opts.resource)
        else: print 'Parameters missing'

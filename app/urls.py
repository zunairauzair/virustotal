from django.views.decorators.csrf import csrf_exempt   
from django.conf import settings
from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns('',
    url(r'^$', 					csrf_exempt(Index.as_view()), 							name='index'),
)

from django.views.generic.base import RedirectView
from django.core.context_processors import csrf
from django.conf import settings

from django.template.response import TemplateResponse
from virus_total_API import VirusTotalAPI

import helper as _
import json

class Index(RedirectView):
    context = {}
    template = 'index.html'

    def get(self, request, *args, **kwargs):
        self.context  = _.default_context()
        self.response = _.default_response()
        template = 'index.html'
        response = TemplateResponse(request, template, self.context)
        return response

    def post(self, request, *args, **kwargs):
        self.context  = _.default_context()
        self.response = _.default_response()

        vt = VirusTotalAPI()
        try: task = request.POST['task']
        except KeyError: task = ''

        if task == 'scan-url':
            try: url = request.POST['url']
            except KeyError: url = ''

            if url != '':
                self.response['data'] = vt.scan_url(url)
                self.response['statusCode']    = 200
                self.response['status']        = True

        elif task == 'url-scan-report':
            try: resource = request.POST['resource']
            except KeyError: resource = ''
            
            if resource != '':
                self.response['data'] = vt.scan_url_report(resource)
                self.response['statusCode']    = 200
                self.response['status']        = True

        elif task == 'scan-file':
            try: fileName = request.POST['fileName']
            except KeyError: fileName = ''

            try: inputFile = request.FILES['file']
            except KeyError: inputFile = ''
            
            if inputFile != '':
                filePath = settings.UPLOAD_DIR + str(inputFile)
                with open(filePath, 'wb+') as destination:
                    for chunk in inputFile.chunks():
                        destination.write(chunk)
                    destination.close()

                self.response['data'] =  json.loads(vt.scan_file(filePath, fileName))
                self.response['statusCode']    = 200
                self.response['status']        = True

        elif task == 'file-scan-report':
            try: resource = request.POST['resource']
            except KeyError: resource = ''
            
            if resource != '':
                self.response['data'] = vt.scan_file_report(resource)
                self.response['statusCode']    = 200
                self.response['status']        = True

        elif task == 'rescan-file':
            try: resource = request.POST['resource']
            except KeyError: resource = ''
            
            if resource != '':
                self.response['data'] = vt.rescan_file(resource)
                self.response['statusCode']    = 200
                self.response['status']        = True

        elif task == 'ip-scan-report':
            try: ip = request.POST['ip']
            except KeyError: ip = ''
            
            if ip != '':
                self.response['data'] = vt.scan_ip_report(ip)
                self.response['statusCode']    = 200
                self.response['status']        = True

        elif task == 'domain-scan-report':
            try: domain = request.POST['domain']
            except KeyError: domain = ''
            
            if domain != '':
                self.response['data'] = vt.scan_domain_report(domain)
                self.response['statusCode']    = 200
                self.response['status']        = True
				
        elif task == 'make-comment':
            try: resource = request.POST['resource']
            except KeyError: resource = ''
            try: comment = request.POST['comment']
            except KeyError: comment = ''

            if resource != '':
                self.response['data'] = vt.make_comment(resource, comment)
                self.response['statusCode']    = 200
                self.response['status']        = True

        return _.serialize_response(self.response)
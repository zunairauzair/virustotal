import postfile
import requests
import json
import urllib

class VirusTotalAPI():

    def __init__(self):
        self.host                    = 'www.virustotal.com'
        self.API_KEY                 = '013541a0f6b117f57ce2951f5978650dd48446065f0a782cc5b3aa27477a88bd'
        self.URLs = {
            'file_scan'   			: 'https://www.virustotal.com/vtapi/v2/file/scan',
            'file_rescan' 			: 'https://www.virustotal.com/vtapi/v2/file/rescan',
            'file_scan_report'    	: 'https://www.virustotal.com/vtapi/v2/file/report',
            'url_scan'    			: 'https://www.virustotal.com/vtapi/v2/url/scan',
            'url_scan_report'       : 'http://www.virustotal.com/vtapi/v2/url/report',
            'ip_scan_report'        : 'http://www.virustotal.com/vtapi/v2/ip-address/report',
            'domain_scan_report'    : 'http://www.virustotal.com/vtapi/v2/domain/report',
            'comment'               : 'https://www.virustotal.com/vtapi/v2/comments/put',
        }

    def scan_file(self, file, file_name):
        fields 		 = [("apikey", self.API_KEY)]
        file_to_send = open(file, "rb").read()
        files        = [("file", file_name, file_to_send)]
        json         = postfile.post_multipart(self.host, self.URLs['file_scan'], fields, files)
        return json

    def rescan_file(self, resource):
        #resource can be md5/sha1/sha256 hash. up to 25 comma separated resource hashes can be provided
        parameters = {"resource": resource,"apikey": self.API_KEY}
        resp       = requests.post(self.URLs['file_rescan'], params=parameters)
        return resp.json()

    def scan_file_report(self, resource):
        #resource can be md5/sha1/sha256 hash. up to 4 comma separated resource hashes can be provided
        parameters = {"resource": resource, "apikey": self.API_KEY}
        resp       = requests.post(self.URLs['file_scan_report'], params=parameters)
        return resp.json()

    def scan_url(self, url):
        #up to up to 4 URLs can be provided for scanning. The URLs must be separated by a new line character.
        parameters = {"url": url,"apikey": self.API_KEY}
        resp       = requests.post(self.URLs['url_scan'], params=parameters)
        return resp.json()
		
    def scan_url_report(self, resource):
        #resource can be scan_id or url name
        parameters = {"resource": resource,"apikey": self.API_KEY}
        resp       = requests.post(self.URLs['url_scan_report'], params=parameters)
        return resp.json()
		
    def scan_ip_report(self, ip):
        #ip can be a valid IPv4 address in dotted quad notation, for the time being only IPv4 addresses are supported.
        parameters = {"ip": ip,"apikey": self.API_KEY}
        response = urllib.urlopen('%s?%s' % (self.URLs['ip_scan_report'], urllib.urlencode(parameters))).read()
        return json.loads(response)

    def scan_domain_report(self, domain):
        #ip can be a valid IPv4 address in dotted quad notation, for the time being only IPv4 addresses are supported.
        parameters = {"domain": domain,"apikey": self.API_KEY}
        response = urllib.urlopen('%s?%s' % (self.URLs['domain_scan_report'], urllib.urlencode(parameters))).read()
        return json.loads(response)

    def make_comment(self, resource, comment):
        parameters = {"resource": resource, "comment": comment, "apikey": self.API_KEY}
        resp       = requests.post(self.URLs['comment'], params=parameters)
        return resp.json()

#vt = VirusTotalAPI()
#res = vt.scan_file("raw.txt", "raw.txt")
#res = vt.rescan_file("f7ddc794cd38f4a4ad1b36d3bec5918e")
#res = vt.file_scan_report("f7ddc794cd38f4a4ad1b36d3bec5918e")
#res = vt.scan_url("http://www.virustotal.com")
#res = vt.scan_ip_report('90.156.201.27')
#res = vt.scan_domain_report('027.ru')
#res = vt.make_comment("f7ddc794cd38f4a4ad1b36d3bec5918e", "Hi, this is test comment")
#print res

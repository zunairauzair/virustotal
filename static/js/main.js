var BASE_URL = '/';

$(document).on('ready',function(){
	
	$('.menuItem').on('click',function(){
		var __this = $(this);
		
		$('.menuItem').removeClass('activeItem');
		__this.addClass('activeItem');
		$('.mainPanel').addClass('hideme');
		$('.'+__this.data('type')).removeClass('hideme');
	});
	
	$('.scanType').on('change',function(){
		$(this).val();
		
		$('.scanForm, .report').addClass('hideme');
		
		if($(this).val() == 'file'){
			$('#scanFileForm').removeClass('hideme');
		}
		else if($(this).val() == 'url'){
			$('#scanURLForm').removeClass('hideme');
		}
	});
	
	
	$('#scanFileForm').on('submit',function(e){
		e.preventDefault();
		
		var form 	  = $(this), 
			formData  = new FormData();
		
		
		$.each(form.find('.file').prop('files'),function(i,file){
			formData.append('file', file);
		});
		
		formData.append('fileName', form.find('.fileName'));
		formData.append('task', 'scan-file');
		
		$.ajax({
			url			: BASE_URL,
		  	cache		: false,
			contentType	: false,
			processData	: false,
			method    	: 'POST',
			data		: formData,
			success		: function(res) {
				console.log(res);
				if(res.status && res.statusCode == 200){
					var panel = $('.scanFileResponse'),
						data  = res.data;
					
					panel.removeClass('hideme');
					
					panel.find('.verbose_msg').html(data.verbose_msg);
					
					if(data.response_code == 1){
						panel.find('.permalink .text').html(data.permalink);
						panel.find('.resource .text').html(data.resource);
						panel.find('.scan_id .text').html(data.scan_id);
						panel.find('.md5 .text').html(data.md5);
						panel.find('.sha1 .text').html(data.sha1);
						panel.find('.sha256 .text').html(data.sha256);
						
						panel.find('.reportDetails').removeClass('hideme');
					}
				}
			},
			error		: function(xhr, status, err) {
				console.error(err);
			}
		});
	});
	
	$('#scanURLForm').on('submit',function(e){
		e.preventDefault();
		var form 	  = $(this);
		
		var data = {
			'task'	: 'scan-url',
			'url'	: form.find('.url').val()
		};
		
		loadAjax (BASE_URL, data, 'POST', function(res){
			if(res.status && res.data.statusCode == 200){
				var panel = $('.scanURLResponse'),
					data  = res.data.data;
				
				panel.removeClass('hideme');
				
				panel.find('.verbose_msg').html(data.verbose_msg);
				
				if(data.response_code == 1){
					panel.find('.permalink .text').html(data.permalink);
					panel.find('.resource .text').html(data.resource);
					panel.find('.url .text').html(data.url);
					panel.find('.scan_id .text').html(data.scan_id);
					panel.find('.scan_date .text').html(data.scan_date);
					panel.find('.reportDetails').removeClass('hideme');
				}
			}
		});
	});
	
	$('#reportsForm').on('submit',function(e){
		e.preventDefault();
		var form 	     = $(this),
			resourceType = form.find('.resourceType').val(),
			data		 = {};
		
		if(resourceType == 'file'){
			data['task'] = 'file-scan-report';
		}
		else if(resourceType == 'url'){
			data['task'] = 'url-scan-report';
		}
		data['resource'] = form.find('.resource').val();
		
		loadAjax (BASE_URL, data, 'POST', function(res){
			
			if(res.status && res.data.statusCode == 200){
				var panel = $('.fileReport'),
					data  = res.data.data;
				
				
				panel.removeClass('hideme');
				panel.find('.verbose_msg').html(data.verbose_msg);
				
				if(data.response_code == 1){
					panel.find('.permalink .text').html(data.permalink);
					panel.find('.resource .text').html(data.resource);
					panel.find('.scan_id .text').html(data.scan_id);
					panel.find('.scan_date .text').html(data.scan_date);
					panel.find('.md5 .text').html(data.md5);
					panel.find('.sha1 .text').html(data.sha1);
					panel.find('.sha256 .text').html(data.sha256);
					
					panel.find('.total .text').html(data.total);
					panel.find('.positives .text').html(data.positives);
					
					panel.find('.scanDetails').html('');
					$.each(data.scans, function(key, val){
						var item = '';
						$.each(val,function(i,el){
							item += '<div class="'+i+' resItem"><span class="label">'+i+': </span><span class="text">'+el+'</span></div>';
						});
						panel.find('.scanDetails').append('<div class="scansItem"><h3>'+key+'</h3><div class="scansItemDetails">'+item+'<div></div>');

					});
					
					panel.find('.reportDetails').removeClass('hideme');
				}
				else{
					panel.find('.reportDetails').addClass('hideme');
				}
			}
		});
	});
	
});

function loadAjax (url, data, method, callback){
	$.ajax({
		url			: url,
		data		: data,
		cache		: false,
		type		: method,
		success		: function(response) {
			callback({
                data	: response,
                status	: true,
            });			
		},
		error		: function(err){
			callback({
                data	: err,
                status	: false,
            });	
		}
	});
}
var InputDataBox = React.createClass({
		getInitialState: function(){
			return { scanType : '',
					 options  : [
						{ value: '', label: 'Select scan type' },
						{ value: 'file', label: 'File' },
						{ value: 'url', label: 'URL' }
					 ],
					 urlScanData : [],
			};
		},
		handleScanType: function(e){
			var scanType = e.target.value;
			console.log(scanType);
			console.log(this);
			
			if(scanType == 'file'){
			
			}else if(scanType == 'url'){
			
			}else{
			
			}
		},
		handleScanURLSubmit : function(res){
			console.log("here in sub");
			console.log(res);
			if(res.status && res.statusCode == 200){
				this.urlScanData = res.data;
			}				
		},
		render: function() {
			var selectOptions = this.state.options.map(function(opt){
				return (
					<option value={opt.value}>{opt.label}</option>
				);
			});
			return (
			  <div>
				<ScanFileForm url={this.props.url} />
				<ScanURLForm url={this.props.url} onScanURLSubmit={this.handleScanURLSubmit} />
				<ScanURLResult data={this.state.urlScanData} />
			  </div>
			);
		}
	});
	
	var ScanFileForm = React.createClass({
		handleSubmit : function(e){
			e.preventDefault();
			console.log("here");
		},
		render : function(){
			return (
				<form className="scanForm" onSubmit={this.handleSubmit} encType="multipart/form-data">
					<h1>Scan File</h1>
					<label>Upload File to submit for scanning</label>
					<br/>
					<input type="file" />
					<input type="submit" value="Post"/>
				</form>
			);
		}
	});
	var ScanURLForm = React.createClass({
		getInitialState: function(){
			return { url : '',data:[]};
		},
		handleURLChange : function(e){
			this.setState({url:e.target.value});
		},
		handleSubmit : function(e){
			e.preventDefault();
			console.log("here");
			var url = this.state.url.trim();
			if(!url){	return false;	}
			this.setState({url:''});
			
			$.ajax({
			  url		: this.props.url,
			  cache		: false,
			  method    : 'POST',
			  data		: {'task':'scan-url','url':url},
			  success	: function(data) {
				console.log(data);
				this.setState({data: data});
				this.props.onScanURLSubmit(data);
			  }.bind(this),
			  error		: function(xhr, status, err) {
				console.error(this.props.url, status, err.toString());
			  }.bind(this)
			});
		
		},
		render : function(){
			return (
				<div>
					<form className="scanForm" onSubmit={this.handleSubmit} >
						<h1>Scan URL</h1>
						<input type="text" placeholder="Enter URL to scan"  value={this.state.url} onChange={this.handleURLChange} />
						<input type="submit" value="Post"/>
					</form>
					<div>
						
					</div>
				</div>
			);
		}
	});
	var ScanURLResult = React.createClass({
		getInitialState: function(){
			return {
				'permalink'		: '',
				'resource'		: '',
				'response_code' : '',
				'scan_date'		: '',
				'scan_id'		: '',
				'url'			: '',
				'verbose_msg'	: ''
			}
		},
		render : function(){
			console.log(this.state.permalink)
			return (
				<div>
					<h1>URL Scan Report</h1>
					<div className="">
						<div><span>URL: </span>{this.state.url}</div>
						<div><span>Permalink: </span>{this.state.permalink}</div>
						<div><span>Resource: </span>{this.state.resource}</div>
						<div><span>Scan ID: </span>{this.state.scan_id}</div>
						<div><span>Scan Date: </span>{this.state.scan_date}</div>
					</div>
				</div>
			);
		}
	});
	ReactDOM.render(
	  <InputDataBox url="/" />,
	  document.getElementById('content')
	);
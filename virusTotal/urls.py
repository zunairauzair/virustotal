from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'virusTotal.views.home', name='home'),
    url(r'^', include('app.urls', namespace='api')),
    #url(r'^admin/', include(admin.site.urls)),
]